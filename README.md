## Rebtel Android assignment
By: Jonas Hansson (jonne84@gmail.com)

### Scope implemented

In this assignment is a simple Android app implemented that starts of with listing all the worlds countries
as provided by the _REST Countries_ (https://restcountries.eu/) API.
The user can click on each country which will then open a new Activity that displays the country flag as provided by
the ``hjnilsson/country-flags`` Github repository.

The app provides some simple error handling for the country listing as well as a progress indicator when downloading
the countries. The country details page shows a placeholder color-fill while downloading the flag image and can also
show a placeholder image if the flag image is missing or downloading failed for some other reason.

### Design considerations

The app is built using a standard procedure with full screen Activity for country listing and country detail, using
a RecyclerView to render the list of countries.
The list of countries is fetched every time on app startup to ensure up-to-date data.

* `Retrofit` is used to download and parse the REST API response, a Singleton containing the retrofit instance as well
as the API models is located in a separate ``api`` package.
* `RxJava` is used to call the Retrofit execution as well as managing on which threads the network process should
 be executed on and that the response/error should be reported back to Main thread. I might be a little too
 excessive to use RxJava for such a simple project, but it is a powerful tool to have as the app scales up
 and further processing is required on the API result. (e.g filtering, chaining of calls etc).
* `Glide` is used for downloading and caching country flag images.
* Annotation tools such as `lombok` and `butterknife` is used to automate generation of accessor methods in data models
and binding views respectively.

### Points of improvement and future work

* Introducing RetroLambda to enable Java 8:s lambda expressions is a powerful tool for more expressive and clean
codebase, especially for RxJava calls and click-listeners. Another alternative could be to rewrite the codebase into
Kotlin language which is more dynamic and safe than java, and has lambda-expression built in natively.
* Automation of `Parcelable` code generation on the data models is also desirable for cleaner codebase and to easier
 change the data models.
* The details page UI could be iterated more over to introduce more information about the country and using Material
 design for a nicer and more familiar user experience.
* Write unit tests.