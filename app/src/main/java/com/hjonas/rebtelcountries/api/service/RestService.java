package com.hjonas.rebtelcountries.api.service;

import com.hjonas.rebtelcountries.api.countries.CountryService;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jonne on 2017-02-19.
 */
public class RestService {

    private static final String      BASE_API_URL = "https://restcountries.eu/rest/v1/";
    private static       RestService sInstance    = new RestService();

    private Retrofit mRetrofit;

    public static RestService getInstance() {
        return sInstance;
    }

    private RestService() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .client(buildClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private OkHttpClient buildClient() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(logInterceptor);

        return builder.build();
    }

    public CountryService getCountryService() {
        return mRetrofit.create(CountryService.class);
    }
}
