package com.hjonas.rebtelcountries.api.countries;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.ToString;

/**
 * Created by jonne on 2017-02-19.
 */

@Getter
@ToString
public class Country implements Parcelable {
    private String name;
    private String capital;
    private String alpha2Code;
    private String region;
    private long   population;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.capital);
        dest.writeString(this.alpha2Code);
        dest.writeString(this.region);
        dest.writeLong(this.population);
    }

    public Country() {
    }

    protected Country(Parcel in) {
        this.name = in.readString();
        this.capital = in.readString();
        this.alpha2Code = in.readString();
        this.region = in.readString();
        this.population = in.readLong();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel source) {
            return new Country(source);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}
