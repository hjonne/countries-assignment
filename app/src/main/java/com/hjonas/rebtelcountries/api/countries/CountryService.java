package com.hjonas.rebtelcountries.api.countries;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by jonne on 2017-02-19.
 */

public interface CountryService {

    @GET("all")
    Observable<List<Country>> fetchAllCountries();
}
