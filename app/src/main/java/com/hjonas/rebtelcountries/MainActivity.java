package com.hjonas.rebtelcountries;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.hjonas.rebtelcountries.api.countries.Country;
import com.hjonas.rebtelcountries.api.service.RestService;
import com.hjonas.rebtelcountries.ui.CountryAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.countries_recyclerview)
    RecyclerView mRecyclerView;

    @BindView(R.id.progress)
    ProgressBar mProgressBar;

    private Subscription mSubscription;

    private Snackbar mSnackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        fetchData();
    }

    @Override
    protected void onStop() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
        super.onStop();
    }

    /**
     * Fetches data from the countries service while showing a loader,
     * calls rendering of data if successful or showing an error message on failures.
     */
    private void fetchData() {
        showLoader();
        mSubscription =
                RestService.getInstance().getCountryService().fetchAllCountries()
                           .subscribeOn(Schedulers.io())
                           .observeOn(AndroidSchedulers.mainThread())
                           .subscribe(new Action1<List<Country>>() {
                               @Override
                               public void call(List<Country> countries) {
                                   hideLoader();
                                   showCountryList(countries);
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   hideLoader();
                                   handleError(throwable);
                               }
                           });
    }

    private void showCountryList(List<Country> countries) {
        CountryAdapter adapter = new CountryAdapter(countries);
        mRecyclerView.setAdapter(adapter);
    }

    private void handleError(Throwable throwable) {
        throwable.printStackTrace();
        mSnackbar = Snackbar
                .make(mRecyclerView, R.string.error_fetching_countries, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fetchData();
                        mSnackbar.dismiss();
                    }
                });
        mSnackbar.show();
    }

    private void showLoader() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgressBar.setVisibility(View.GONE);
    }
}
