package com.hjonas.rebtelcountries.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hjonas.rebtelcountries.R;
import com.hjonas.rebtelcountries.api.countries.Country;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jonne on 2017-02-21.
 */

public class CountryDetailActivity extends AppCompatActivity {

    private static final String TAG = CountryDetailActivity.class.getSimpleName();

    // FIXME: A production app should not use github as data source since it violates their terms of usage.
    private static final String IMAGES_BASE_URL = "https://raw.githubusercontent.com/hjnilsson/country-flags/master/png1000px/";

    @BindView(R.id.country_details_name)
    TextView mTitleText;

    @BindView(R.id.country_details_region)
    TextView mRegionText;

    @BindView(R.id.details_country_flag_image)
    ImageView mCountryFlagImage;

    @BindView(R.id.details_country_capital)
    TextView mCapitalText;

    @BindView(R.id.details_country_population)
    TextView mPopulationText;

    private static final String INTENT_EXTRA_COUNTRY = "extra.country";

    public static Intent newIntent(Country country, Context context) {
        Intent intent = new Intent(context, CountryDetailActivity.class);
        intent.putExtra(INTENT_EXTRA_COUNTRY, country);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().hasExtra(INTENT_EXTRA_COUNTRY)) {
            Country country = getIntent().getParcelableExtra(INTENT_EXTRA_COUNTRY);
            bindData(country);
        } else {
            Log.e(TAG, "Intent missing!");
        }
    }

    private void bindData(Country country) {
        setTitle(country.getName());

        mTitleText.setText(country.getName());

        if (!TextUtils.isEmpty(country.getRegion())) {
            mRegionText.setText("(" + country.getRegion() + ")");
        } else {
            mRegionText.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(country.getCapital())) {
            mCapitalText.setText(country.getCapital());
        } else {
            mCapitalText.setText(R.string.placeholder_missing);
        }

        String populationFormatted = String.format(Locale.ENGLISH, "%,d", country.getPopulation())
                                           .replace(",", " ");
        mPopulationText.setText(String.valueOf(populationFormatted));

        String imageUrl = IMAGES_BASE_URL + country.getAlpha2Code().toLowerCase() + ".png";
        Glide.with(this).load(imageUrl)
             .error(R.drawable.placeholder_flag)
             .placeholder(R.drawable.loader_placeholer)
             .fitCenter()
             .into(mCountryFlagImage);
    }
}
