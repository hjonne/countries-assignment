package com.hjonas.rebtelcountries.ui;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hjonas.rebtelcountries.R;
import com.hjonas.rebtelcountries.api.countries.Country;

import java.util.List;

/**
 * Created by jonne on 2017-02-19.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> {

    private List<Country> mCountryList;

    public CountryAdapter(List<Country> countryList) {
        mCountryList = countryList;
    }

    @Override
    public CountryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_list_item, null, false);
        return new CountryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CountryViewHolder holder, int position) {
        final Country item = mCountryList.get(position);

        holder.countryNameText.setText(item.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = CountryDetailActivity.newIntent(item, holder.itemView.getContext());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCountryList.size();
    }

    static class CountryViewHolder extends RecyclerView.ViewHolder {

        final TextView countryNameText;

        public CountryViewHolder(View itemView) {
            super(itemView);

            countryNameText = (TextView) itemView.findViewById(R.id.country_name);
        }
    }
}
